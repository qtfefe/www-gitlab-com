---
layout: markdown_page
title: "FedRAMP and GitLab"
description: "Specifically, FedRAMP is focused on cloud products and services, evaluating and certifying cloud PAAS, IAAS, and SAAS offerings. Learn more here!"
canonical_path: "/solutions/public-sector/fedramp/"
---

## What is FedRAMP?

The Federal Risk and Authorization Management Program (FedRAMP) is a government-wide program that provides a standardized approach to security assessment, authorization, and continuous monitoring for cloud products and services. [See the GSA definition.](https://www.gsa.gov/technology/government-it-initiatives/fedramp)   

Details about the [FedRAMP program](https://www.fedramp.gov/) highlight the process and status of how cloud services are assessed and certified in the [FedRAMP marketplace.](https://marketplace.fedramp.gov/#/products?sort=productName)

## GitLab and FedRAMP

[FedRAMP authorization](https://www.fedramp.gov/) is applicable only to **Cloud Service Offerings**, as a significant focus of the controls are on operational aspects of a service. In the context of GitLab products, it is only applicable to GitLab SaaS Services. 

GitLab is pursuing FedRAMP Moderate authorization for a SaaS service we provide and host. When we have an expected timeline for achieving FedRAMP-authorized status, we will add it to our product roadmap.  

In the meantime, customers are able to deploy GitLab into their FedRAMP authorization boundary including [AWS](https://docs.gitlab.com/ee/install/aws/){:data-ga-name="AWS"}{:data-ga-location="body"}, [Google Cloud](https://docs.gitlab.com/ee/install/google_cloud_platform/index.html){:data-ga-name="Google cloud"}{:data-ga-location="body"}, [Azure](https://docs.gitlab.com/ee/install/azure/index.html){:data-ga-name="Azure"}{:data-ga-location="body"}, or on-prem/data center.  GitLab provides [documentation](https://docs.gitlab.com/ee/development/fips_compliance.html#fips-compliance) on how to install a FIPS-compliant version of our software.

If you want to learn more about GitLab and how we support public sector agencies, departments, and organizations, please contact us.

<center><a href="/solutions/public-sector/#contact" class="btn cta-btn orange" data-ga-name="Contact us" data-ga-location="body">contact our public sector team</a></center>
