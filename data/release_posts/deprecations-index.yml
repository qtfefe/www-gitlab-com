---
'16.8':
- "`after_script` keyword will run for cancelled jobs"
- License Scanning support for sbt 1.0.X
- License List is deprecated
- Dependency Scanning support for sbt 1.0.X
- "`metric` filter and `value` field for DORA API"
- Support for setting custom schema for backup is deprecated
- Block usage of ref and sha together in `GET /projects/:id/ci/lint`
- GitLab Runner provenance metadata SLSA v0.2 statement
'16.7':
- List repository directories Rake task
- JWT `/-/jwks` instance endpoint is deprecated
- 'Dependency Proxy: Access tokens to have additional scope checks'
'16.6':
- The GitHub importer Rake task
- Proxy-based DAST deprecated
- Breaking change to the Maven repository group permissions
- 'GraphQL: deprecate support for `canDestroy` and `canDelete`'
- Legacy Geo Prometheus metrics
- File type variable expansion fixed in downstream pipelines
- Container registry support for the Swift and OSS storage drivers
'16.5':
- Offset pagination for `/users` REST API endpoint is deprecated
- Security policy field `newly_detected` is deprecated
'16.4':
- Internal container registry API tag deletion endpoint
- "`postgres_exporter['per_table_stats']` configuration setting"
- The `ci_job_token_scope_enabled` projects API attribute is deprecated
- 'Geo: Legacy replication details routes for designs and projects deprecated'
- Deprecate change vulnerability status from the Developer role
'16.3':
- 'Geo: Housekeeping Rake tasks'
- Deprecate field `hasSolutions` from GraphQL VulnerabilityType
- Twitter OmniAuth login option is deprecated from self-managed GitLab
- RSA key size limits
- Twitter OmniAuth login option is removed from GitLab.com
- Job token allowlist covers public and internal projects
- GraphQL field `totalWeight` is deprecated
'16.2':
- GraphQL field `registrySizeEstimated` has been deprecated
- OmniAuth Facebook is deprecated
- Deprecated parameters related to custom text in the sign-in page
- Deprecate `CiRunner` GraphQL fields duplicated in `CiRunnerManager`
- The pull-based deployment features of the GitLab agent for Kubernetes is deprecated
'16.1':
- Deprecate Windows CMD in GitLab Runner
- Unified approval rules are deprecated
- Deprecate `message` field from Vulnerability Management features
- Running a single database is deprecated
- GraphQL deprecation of `dependencyProxyTotalSizeInBytes` field
'16.0':
- GitLab administrators must have permission to modify protected branches or tags
- GraphQL type, `RunnerMembershipFilter` renamed to `CiRunnerMembershipFilter`
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
- CiRunnerUpgradeStatusType GraphQL type renamed to CiRunnerUpgradeStatus
- "`sidekiq` delivery method for `incoming_email` and `service_desk_email` is deprecated"
- PostgreSQL 13 deprecated
- Bundled Grafana deprecated and disabled
- CiRunner.projects default sort is changing to `id_desc`
'15.9':
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- Trigger jobs can mirror downstream pipeline status exactly
- HashiCorp Vault integration will no longer use CI_JOB_JWT by default
- Legacy URLs replaced or removed
- Secure scanning CI/CD templates will use new job `rules`
- Secure analyzers major version update
- SAST analyzer coverage changing in GitLab 16.0
- Support for Praefect custom metrics endpoint configuration
- Development dependencies reported for PHP and Python
- Required Pipeline Configuration is deprecated
- "`omniauth-authentiq` gem no longer available"
- License Compliance CI Template
- License-Check and the Policies tab on the License Compliance page
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Legacy Praefect configuration method
- Managed Licenses API
- Filepath field in Releases and Release Links APIs
- Embedding Grafana panels in Markdown is deprecated
- Queue selector for running Sidekiq is deprecated
- GitLab Runner platforms and setup instructions in GraphQL API
- Option to delete projects immediately is deprecated from deletion protection settings
- External field in Releases and Release Links APIs
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- Deprecation and planned removal for `CI_PRE_CLONE_SCRIPT` variable on GitLab SaaS
- External field in GraphQL ReleaseAssetLink type
- Enforced validation of CI/CD parameter character lengths
- Single database connection is deprecated
- Old versions of JSON web tokens are deprecated
- Slack notifications integration
'15.8':
- Use of third party container registries is deprecated
- The Visual Reviews tool is deprecated
- Configuring Redis config file paths using environment variables is deprecated
- "`environment_tier` parameter for DORA API"
- The latest Terraform templates will overwrite current stable templates
- Developer role providing the ability to import projects to a group
- The API no longer returns revoked tokens for the agent for Kubernetes
- Non-standard default Redis ports are deprecated
- Projects API field `operations_access_level` is deprecated
- Container registry pull-through cache
- Maintainer role providing the ability to change Package settings using GraphQL API
- Live Preview no longer available in the Web IDE
- Cookie authorization in the GitLab for Jira Cloud app
- Deployment API returns error when `updated_at` and `updated_at` are not used together
- GitLab Helm chart values `gitlab.kas.privateApi.*` are deprecated
- Auto DevOps support for Herokuish is deprecated
- 'GraphQL: The `DISABLED_WITH_OVERRIDE` value of the `SharedRunnersSetting` enum
  is deprecated. Use `DISABLED_AND_OVERRIDABLE` instead'
- Automatic backup upload using Openstack Swift and Rackspace APIs
- Limit personal access token and deploy token's access with external authorization
- Conan project-level search endpoint returns project-specific results
- Auto DevOps no longer provisions a PostgreSQL database by default
- Dependency Scanning support for Java 13, 14, 15, and 16
- Azure Storage Driver defaults to the correct root prefix
'15.7':
- "`POST ci/lint` API endpoint deprecated"
- DAST report variables deprecation
- Support for periods (`.`) in Terraform state names might break existing states
- Shimo integration
- ZenTao integration
- KAS Metrics Port in GitLab Helm Chart
- The Phabricator task importer is deprecated
- The `gitlab-runner exec` command is deprecated
- DAST ZAP advanced configuration variables deprecation
- Support for REST API endpoints that reset runner registration tokens
- DAST API scans using DAST template is deprecated
- DAST API variables
'15.6':
- "`runnerRegistrationToken` parameter for GitLab Runner Helm Chart"
- GitLab Runner registration token in Runner Operator
- Registration tokens and server-side runner arguments in `POST /api/v4/runners` endpoint
- Registration tokens and server-side runner arguments in `gitlab-runner register`
  command
- Configuration fields in GitLab Runner Helm Chart
'15.5':
- GraphQL field `confidential` changed to `internal` on notes
- vulnerabilityFindingDismiss GraphQL mutation
- File Type variable expansion in `.gitlab-ci.yml`
'15.4':
- Starboard directive in the config for the GitLab Agent for Kubernetes
- Container Scanning variables that reference Docker
- Non-expiring access tokens
- Vulnerability confidence field
- Toggle behavior of `/draft` quick action in merge requests
'15.3':
- Security report schemas version 14.x.x
- Use of `id` field in vulnerabilityFindingDismiss mutation
- Atlassian Crowd OmniAuth provider
- CAS OmniAuth provider
- Redis 5 deprecated
'15.2':
- Remove `job_age` parameter from `POST /jobs/request` Runner endpoint
'15.10':
- Environment search query requires at least three characters
- Major bundled Helm Chart updates for the GitLab Helm Chart
- DingTalk OmniAuth provider
- Deprecated Consul http metrics
- Bundled Grafana Helm Chart is deprecated
- Work items path with global ID at the end of the path is deprecated
- Legacy Gitaly configuration method
'15.1':
- PipelineSecurityReportFinding projectFingerprint GraphQL field
- Jira DVCS connector for Jira Cloud
- project.pipeline.securityReportFindings GraphQL query
- PipelineSecurityReportFinding name GraphQL field
'15.0':
- GraphQL API legacyMode argument for Runner status
- PostgreSQL 12 deprecated
'14.9':
- GitLab self-monitoring project
- Integrated error tracking disabled by default
- htpasswd Authentication for the container registry
- "`user_email_lookup_limit` API field"
- GraphQL permissions change for Package settings
- Permissions change for downloading Composer dependencies
- Background upload for object storage
'14.8':
- Querying Usage Trends via the `instanceStatisticsMeasurements` GraphQL node
- OAuth tokens without expiration
- Test coverage project CI/CD setting
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Secure and Protect analyzer images published in new location
- Secure and Protect analyzer major version update
- SAST support for .NET 2.1
- Out-of-the-box SAST support for Java 8
- SAST analyzer consolidation and CI/CD template changes
- Request profiling
- Vulnerability Check
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- "`started` iteration state"
- Container Network and Host Security
- GraphQL ID and GlobalID compatibility
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- GraphQL networkPolicies resource deprecated
- Deprecate legacy Gitaly configuration methods
- Optional enforcement of SSH expiration
- Optional enforcement of PAT expiration
- "`projectFingerprint` in `PipelineSecurityReportFinding` GraphQL"
- Retire-JS Dependency Scanning tool
- "`CI_BUILD_*` predefined variables"
- External status check API breaking changes
- Required pipeline configurations in Premium tier
- Elasticsearch 6.8
'14.7':
- Tracing in GitLab
- Logging in GitLab
- Monitor performance metrics through Prometheus
- Sidekiq metrics and health checks configuration
- "`artifacts:reports:cobertura` keyword"
'14.6':
- Legacy approval status names from License Compliance API
- bundler-audit Dependency Scanning tool
- apiFuzzingCiConfigurationCreate GraphQL mutation
- CI/CD job name length limit
- "`type` and `types` keyword in CI/CD configuration"
'14.5':
- "`pipelines` field from the `version` field"
- GraphQL API Runner status will not return `paused`
- Package pipelines in API payload is paginated
- "`dependency_proxy_for_private_groups` feature flag"
- Update to the container registry group-level API
- "`promote-to-primary-node` command from `gitlab-ctl`"
- "`promote-db` command from `gitlab-ctl`"
- "`defaultMergeCommitMessageWithDescription` GraphQL API field"
- Self-managed certificate-based integration with Kubernetes
- Known host required for GitLab Runner SSH executor
- Changing an instance (shared) runner to a project (specific) runner
- Value Stream Analytics filtering calculation change
- SaaS certificate-based integration with Kubernetes
- "`Versions` on base `PackageType`"
- Support for SLES 12 SP2
'14.3':
- GitLab Serverless
- OmniAuth Kerberos gem
- Legacy database configuration
- Audit events for repository push events
'14.10':
- Outdated indices of Advanced Search migrations
- Dependency Scanning default Java version changed to 17
- Toggle notes confidentiality on APIs
'14.0':
- OAuth implicit grant
- Changing merge request approvals with the `/approvals` API endpoint
