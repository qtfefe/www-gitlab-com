---
features:
  secondary:
  - name: "Comprehensive list of items that failed to be imported"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/group/import/index.html#review-results-of-the-import'
    reporter: m_frankiewicz
    stage: manage
    categories:
    - Importers
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/386138'
    description: |
      Previously, when migrating GitLab projects and groups by direct transfer had completed and some items (such as a merge requests or issues) were not
      successfully imported, you could select a **Details** button on the
      [page listing imported groups and projects](https://docs.gitlab.com/ee/user/group/import/index.html#group-import-history) and see related errors there.

      However, a list of errors is not helpful to understand how many items in total, and which items in particular, were not imported. Having this
      information is crucial to understanding the results of the import process.
      
      In this release, we replaced the **Details** button with a **See failures** link. Selecting the **See failures** link takes you to a new page listing all items that failed
      to import for a given group or project. For each item that wasn't imported, you can see:

      - The type of the item. For example, merge request or issue.
      - What kind of error occurred.
      - The correlation ID, which is useful for debugging purposes.
      - The URL of the item on the source instance, if available (items with `iid`).
      - The title of the item on the source instance, if available. For example, the merge request title or the issue title.
